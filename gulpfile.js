const { src, dest, series, parallel, watch, lastRun } = require('gulp');

const iconfont = 		require('gulp-iconfont');
const iconfontCss = 	require('gulp-iconfont-css');

const imagemin = 		require('gulp-imagemin');
const webp = 			require('imagemin-webp');
const extReplace = 		require('gulp-ext-replace');

const autocrop = 		require('gulp-autocrop');
const rename = 			require('gulp-rename');

function fontTask (cb) {

	var fontName = 'icons';

	src('assets/font/*.svg', { since: lastRun(fontTask, 1000) })
		.pipe(imagemin([
			imagemin.svgo({
				plugins: [
					{ cleanupNumericValues: 0 }
				]
			})
		]))
		.pipe(iconfontCss({
			fontName: fontName,
			path: 'assets/font/.icons.template.css',
			targetPath: '../css/icons.css',
			fontPath: '../fonts/',
			cacheBuster: Math.round(+new Date / 1e4)
		}))
		.pipe(iconfont({
			fontName: fontName,
			//prependUnicode: true,
			formats: ['ttf', 'woff2', 'woff']
		}))
		.pipe(dest('assets/fonts'))
		.on('finish', cb);
}


function iconTask (cb) {
	const sizes = [32, 64, 68, 76, 120, 128, 152, 256, 512];
	let count = 0;
	sizes.forEach(function (size) {
		src('assets/icon.png', { since: lastRun(iconTask, 1000) })
			.pipe(autocrop({
				height: size,
				width: size
			}))
			.pipe(rename({
				basename: size
			}))
			.pipe(imagemin())
			.pipe(dest('./assets/icons'))
			.on('finish', () => {
				count+=1;
				if(count === sizes.length)
					cb();
			})
	})
}


function webpTask (cb) {
	src('assets/images/**/*.png')
		.pipe(imagemin([
			webp({
				quality: 90
			})
		]))
		.pipe(extReplace('.webp'))
		.pipe(dest('assets/images'))
		.on('finish', cb);
}


exports.default = parallel(fontTask, iconTask, webpTask);
